package nl.kza;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by Wluijk on 2/16/2017.
 */
public class MyStepdefs {


    private WebDriver driver;


    @Before
    public void beforeScenario() throws Throwable {

        //Prepare relevant firefox driver
        //TODO: Get a shared driver
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Wluijk\\geckodriver.exe");
        ProfilesIni profile = new ProfilesIni();
        FirefoxProfile ffprofile = profile.getProfile("Selenium");
        driver = new FirefoxDriver(ffprofile);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
    }

    @Given("^I have an internet connection$")
    public void iHaveAnInternetConnection() throws Throwable {
        //Set the ipadress of www.google.nl
        String ipAddress = "216.58.212.227";
        InetAddress inet = InetAddress.getByName(ipAddress);

        //Deterimine if its reachable
        boolean reachable = inet.isReachable(5000);

        //Assert its reachable, otherwise the test fails
        assertTrue(reachable);

        //Report on outcome
        System.out.println("Sending Ping Request to " + ipAddress);
        System.out.println(reachable ? "Host is reachable" : "Host is NOT reachable");
    }

    @When("^I open \"([^\"]*)\"$")
    public void iOpen(String url) throws Throwable {
        //Open de provided url in Firefox
        driver.get(url);


    }

    @Then("^I want to get the search page$")
    public void iWantToGetTheSearchPage() throws Throwable {
        //Detirmine if title is google
        boolean titleIsGoogle = (driver.getTitle().equals("Google"));


        //Assert if title is google
        assertTrue(titleIsGoogle);

        // Report on outcome
        System.out.println(titleIsGoogle ? "Test succesfull: page title is Google" : "Test failed");
    }


    @After
    public void afterScenario() throws Throwable {
        //Ugly wait of 12 seconds to look at the result of browsing
        Thread.sleep(12000);

        //Shutdown firefox
        driver.close();
        driver.quit();
    }

}
