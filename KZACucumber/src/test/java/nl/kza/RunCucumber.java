package nl.kza;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Wluijk on 2/16/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        glue = "nl.kza",
        features = "classpath:cucumber/test.feature")


public class RunCucumber {
}
